import * as http from 'http';
import * as express from 'express';
import * as request from 'request';
import * as bodyParser from 'body-parser';
import * as JiraClient from 'jira-connector';
import * as _ from 'lodash';
import * as moment from 'moment';
import {AddressInfo} from 'net';

let router = express();

router.all('/webHook', function (req, res) {
    req.pipe(request({
        url: 'https://' + req.query.domain + '/app/site/hosting/scriptlet.nl?script=' + req.query.script + '&deploy=' + req.query.deploy + '&compid=' + req.query.compid + '&h=' + req.query.h,
        headers: {
            'User-Agent': 'Mozilla/5.0'
        }
    })).pipe(res);
});

router.use('/brickFTP', function (req, res) {
    let netsuiteWebHookURL = 'https://forms.netsuite.com/app/site/hosting/scriptlet.nl?script=5&deploy=1&compid=TSTDRV1381486&h=d0c6069a4035d758306b';
    for (let key in req.query) {
        netsuiteWebHookURL += '&' + key + '=' + req.query[key];
    }
    req.pipe(request({
        url: netsuiteWebHookURL,
        headers: {
            'User-Agent': 'Mozilla/5.0'
        }
    })).pipe(res);
});

//noinspection JSCheckFunctionSignatures
router.use(bodyParser.json()); // for parsing application/json

//noinspection JSCheckFunctionSignatures
router.use(bodyParser.urlencoded({
    extended: true
})); // for parsing application/x-www-form-urlencoded

let server = http.createServer(router);

router.get('/', function (req, res) {
    res.send(200);
});

router.post('/worklog', function (req, res) {
    let workLogData = req.body;
    let URL_Regex = /https:\/\/darrenhillconsulting\.atlassian\.net\/rest\/api\/2\/issue\/(\d+)\/worklog\/(\d+)/;
    let issueID = workLogData.worklog.self.match(URL_Regex)[1];
    let authtoken = 'ac3565fee6c6efb64294d65edca6627b';
    let organization_id = '285548441';
    let userId = '277345000000044001';

    let jira = new JiraClient({
        host: 'darrenhillconsulting.atlassian.net',
        basic_auth: {
            username: 'darrenhillconsulting',
            password: '!ace1RELIC'
        }
    });

    jira.issue.getIssue({
        issueId: issueID // Get this from the worklog.self url  =  "https://darrenhillconsulting.atlassian.net/rest/api/2/issue/12401/worklog/12714"
    }, function (error, issue) {
        if (error) {
            console.log(error);
        }

        if (issue) {
            // Find the project by Name (do so by fetching all projects)
            request('https://books.zoho.com/api/v3/projects?authtoken=' + authtoken + '&organization_id=' + organization_id, function (error, response, body) {
                if (!error && +response.statusCode === 200) {
                    let zohoProjects = JSON.parse(body);

                    let projectId = _.result(_.find(zohoProjects.projects, function (project) {
                        //noinspection JSUnresolvedVariable
                        return project.project_name.indexOf(issue.fields.customfield_10900.value) !== -1;
                    }), 'project_id');

                    // Find the task by name (search for keywords in the JIRA issue message.  Support, Met/Meet, etc);
                    if (projectId) {
                        request('https://books.zoho.com/api/v3/projects/' + projectId + '/tasks?authtoken=' + authtoken + '&organization_id=' + organization_id, function (error, response, body) {
                            if (!error && +response.statusCode === 200) {
                                let zohoProjectTasks = JSON.parse(body);
                                let TASKNAME = 'Development';

                                if (workLogData.worklog.comment.indexOf('MEETING') !== -1) {
                                    TASKNAME = 'Meetings';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('MEETING ', '');
                                } else if (workLogData.worklog.comment.indexOf('FIELD MAPPING') !== -1) {
                                    TASKNAME = 'Field Mapping';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('FIELD MAPPING ', '');
                                } else if (workLogData.worklog.comment.indexOf('TESTING') !== -1) {
                                    TASKNAME = 'Testing';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('TESTING ', '');
                                } else if (workLogData.worklog.comment.indexOf('SUPPORT') !== -1) {
                                    TASKNAME = 'Support';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('SUPPORT ', '');
                                } else if (workLogData.worklog.comment.indexOf('MISC') !== -1) {
                                    TASKNAME = 'Misc';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('MISC ', '');
                                }

                                let taskId = _.result(_.find(zohoProjectTasks.task, function (taskInfo) {
                                    //noinspection JSUnresolvedVariable
                                    return taskInfo.task_name === TASKNAME;
                                }), 'task_id');

                                if (taskId) {
                                    //noinspection JSUnresolvedVariable
                                    let jiraMilliseconds = workLogData.worklog.timeSpentSeconds * 1000;

                                    //noinspection JSUnresolvedVariable
                                    let zohoBookTimeEntry = {
                                        project_id: projectId,
                                        task_id: taskId,
                                        user_id: userId,
                                        log_date: workLogData.worklog.started.split('T')[0],
                                        begin_time: '',
                                        end_time: '',
                                        log_time: moment.utc(jiraMilliseconds).format('HH:mm'),
                                        is_billable: true,
                                        notes: issue.key + ' : ' + workLogData.worklog.comment,
                                        start_timer: ''
                                    };

                                    request({
                                        method: 'POST',
                                        uri: 'https://books.zoho.com/api/v3/projects/timeentries?authtoken=' + authtoken + '&organization_id=' + organization_id + '&JSONString= ' + JSON.stringify(zohoBookTimeEntry)
                                    }, function (error, response, body) {
                                        console.log(body);
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
        else {
            console.log('No Zoho Books project for this issue!');
        }
    });
    res.json(req.body);
});

server.listen(process.env.PORT || 3000, +process.env.IP, function () {
    let addr: AddressInfo = <AddressInfo>server.address();
    console.log('Chat server listening at', addr.address + ':' + addr.port);
});
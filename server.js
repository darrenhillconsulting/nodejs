Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var express = require("express");
var request = require("request");
var bodyParser = require("body-parser");
var JiraClient = require("jira-connector");
var _ = require("lodash");
var moment = require("moment");
var router = express();
router.all('/webHook', function (req, res) {
    req.pipe(request({
        url: 'https://' + req.query.domain + '/app/site/hosting/scriptlet.nl?script=' + req.query.script + '&deploy=' + req.query.deploy + '&compid=' + req.query.compid + '&h=' + req.query.h,
        headers: {
            'User-Agent': 'Mozilla/5.0'
        }
    })).pipe(res);
});
router.use('/brickFTP', function (req, res) {
    var netsuiteWebHookURL = 'https://forms.netsuite.com/app/site/hosting/scriptlet.nl?script=5&deploy=1&compid=TSTDRV1381486&h=d0c6069a4035d758306b';
    for (var key in req.query) {
        netsuiteWebHookURL += '&' + key + '=' + req.query[key];
    }
    req.pipe(request({
        url: netsuiteWebHookURL,
        headers: {
            'User-Agent': 'Mozilla/5.0'
        }
    })).pipe(res);
});
//noinspection JSCheckFunctionSignatures
router.use(bodyParser.json()); // for parsing application/json
//noinspection JSCheckFunctionSignatures
router.use(bodyParser.urlencoded({
    extended: true
})); // for parsing application/x-www-form-urlencoded
var server = http.createServer(router);
router.get('/', function (req, res) {
    res.send(200);
});
router.post('/worklog', function (req, res) {
    var workLogData = req.body;
    var URL_Regex = /https:\/\/darrenhillconsulting\.atlassian\.net\/rest\/api\/2\/issue\/(\d+)\/worklog\/(\d+)/;
    var issueID = workLogData.worklog.self.match(URL_Regex)[1];
    var authtoken = 'ac3565fee6c6efb64294d65edca6627b';
    var organization_id = '285548441';
    var userId = '277345000000044001';
    var jira = new JiraClient({
        host: 'darrenhillconsulting.atlassian.net',
        basic_auth: {
            username: 'darrenhillconsulting',
            password: '!ace1RELIC'
        }
    });
    jira.issue.getIssue({
        issueId: issueID // Get this from the worklog.self url  =  "https://darrenhillconsulting.atlassian.net/rest/api/2/issue/12401/worklog/12714"
    }, function (error, issue) {
        if (error) {
            console.log(error);
        }
        if (issue) {
            // Find the project by Name (do so by fetching all projects)
            request('https://books.zoho.com/api/v3/projects?authtoken=' + authtoken + '&organization_id=' + organization_id, function (error, response, body) {
                if (!error && +response.statusCode === 200) {
                    var zohoProjects = JSON.parse(body);
                    var projectId_1 = _.result(_.find(zohoProjects.projects, function (project) {
                        //noinspection JSUnresolvedVariable
                        return project.project_name.indexOf(issue.fields.customfield_10900.value) !== -1;
                    }), 'project_id');
                    // Find the task by name (search for keywords in the JIRA issue message.  Support, Met/Meet, etc);
                    if (projectId_1) {
                        request('https://books.zoho.com/api/v3/projects/' + projectId_1 + '/tasks?authtoken=' + authtoken + '&organization_id=' + organization_id, function (error, response, body) {
                            if (!error && +response.statusCode === 200) {
                                var zohoProjectTasks = JSON.parse(body);
                                var TASKNAME_1 = 'Development';
                                if (workLogData.worklog.comment.indexOf('MEETING') !== -1) {
                                    TASKNAME_1 = 'Meetings';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('MEETING ', '');
                                }
                                else if (workLogData.worklog.comment.indexOf('FIELD MAPPING') !== -1) {
                                    TASKNAME_1 = 'Field Mapping';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('FIELD MAPPING ', '');
                                }
                                else if (workLogData.worklog.comment.indexOf('TESTING') !== -1) {
                                    TASKNAME_1 = 'Testing';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('TESTING ', '');
                                }
                                else if (workLogData.worklog.comment.indexOf('SUPPORT') !== -1) {
                                    TASKNAME_1 = 'Support';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('SUPPORT ', '');
                                }
                                else if (workLogData.worklog.comment.indexOf('MISC') !== -1) {
                                    TASKNAME_1 = 'Misc';
                                    workLogData.worklog.comment = workLogData.worklog.comment.replace('MISC ', '');
                                }
                                var taskId = _.result(_.find(zohoProjectTasks.task, function (taskInfo) {
                                    //noinspection JSUnresolvedVariable
                                    return taskInfo.task_name === TASKNAME_1;
                                }), 'task_id');
                                if (taskId) {
                                    //noinspection JSUnresolvedVariable
                                    var jiraMilliseconds = workLogData.worklog.timeSpentSeconds * 1000;
                                    //noinspection JSUnresolvedVariable
                                    var zohoBookTimeEntry = {
                                        project_id: projectId_1,
                                        task_id: taskId,
                                        user_id: userId,
                                        log_date: workLogData.worklog.started.split('T')[0],
                                        begin_time: '',
                                        end_time: '',
                                        log_time: moment.utc(jiraMilliseconds).format('HH:mm'),
                                        is_billable: true,
                                        notes: issue.key + ' : ' + workLogData.worklog.comment,
                                        start_timer: ''
                                    };
                                    request({
                                        method: 'POST',
                                        uri: 'https://books.zoho.com/api/v3/projects/timeentries?authtoken=' + authtoken + '&organization_id=' + organization_id + '&JSONString= ' + JSON.stringify(zohoBookTimeEntry)
                                    }, function (error, response, body) {
                                        console.log(body);
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
        else {
            console.log('No Zoho Books project for this issue!');
        }
    });
    res.json(req.body);
});
server.listen(process.env.PORT || 3000, +process.env.IP, function () {
    var addr = server.address();
    console.log('Chat server listening at', addr.address + ':' + addr.port);
});
